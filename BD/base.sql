
CREATE TABLE estado (
                id INT AUTO_INCREMENT NOT NULL,
                UF VARCHAR(2) NOT NULL,
                nome VARCHAR(50) NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE cidade (
                id INT AUTO_INCREMENT NOT NULL,
                nome VARCHAR(30) NOT NULL,
                id_estado INT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE pessoa (
                id INT AUTO_INCREMENT NOT NULL,
                id_cidade INT NOT NULL,
                nome VARCHAR(50) NOT NULL,
                idade INT NOT NULL,
                PRIMARY KEY (id)
);


ALTER TABLE cidade ADD CONSTRAINT estado_cidade_fk
FOREIGN KEY (id_estado)
REFERENCES estado (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE pessoa ADD CONSTRAINT cidade_pessoa_fk
FOREIGN KEY (id_cidade)
REFERENCES cidade (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;
