<?php

    // require_once '../modelo/cidade_modelo.php';
    // require_once './controlador_base.php';
    /**
     *
     */
    class Cidade extends Controlador_Base{
        // private $modelo;
        private $nome;
        private $link;

        function __construct() {
            // strtolower(get_class($this))
            $this->nome = "cidade"; //nome da classe genérico
            $this->link = '?controlador=' . $this->nome;
            //$this->modelo = new Cidade_Model();
            $this->loadModel('Cidade_Modelo');
        }

        public function index() {
            $link  = $this->link;
            $lista = $this->modelo->get();
            require $GLOBALS['APPPATH'] . '/visao/'.$this->nome.'/lista_'.$this->nome.'.php';
        }

        public function cadastrar($id=NULL) {
            $listaEstados = $this->modelo->getLista('estado'); //Receber dados da tabela estado

            $acao = $this->link . '&acao=gravar';
            if($id!=NULL){
                $registro = $this->modelo->getById($id);
                // var_dump($registro); die();
            }
            require $GLOBALS['APPPATH'] . '/visao/'.$this->nome.'/form_'.$this->nome.'.php';
        }


        public function gravar() {
            $dados = $_POST;
            if($_REQUEST['id']){
                $dados['id'] = $_REQUEST['id'];
            }
            // print_r($dados); die();
            $r = $this->modelo->inserir($dados);
            $this->redirect('?controlador='.$this->nome);
            // header('location: ./cidade.php');
        }

        public function remover($id) {
            // echo $id; die();
            $r = $this->modelo->remover($id);
            $this->redirect('?controlador='.$this->nome);
            // header('location: ./cidade.php');
        }
    }

    //instância do objeto
    // $cidade = new Cidade();
    //
    // $acao = isset($_REQUEST['acao'])? $_REQUEST['acao'] : 'listar';
    // $id   = isset($_REQUEST['id'])? $_REQUEST['id'] : null;
    // $cidade->{$acao}($id);

 ?>
