<?php

    // require_once '../modelo/cidade_modelo.php';
    // require_once './controlador_base.php';
    /**
     *
     */
    class Pessoa extends Controlador_Base{
        // private $modelo;
        private $nome;
        private $link;

        function __construct() {
            // strtolower(get_class($this))
            $this->nome = "pessoa"; //nome da classe genérico
            $this->link = '?controlador=' . $this->nome;
            //$this->modelo = new Cidade_Model();
            $this->loadModel('Pessoa_Modelo');
        }

        public function index() {
            $link  = $this->link;
            $lista = $this->modelo->get();
            require $GLOBALS['APPPATH'] . '/visao/'.$this->nome.'/lista_'.$this->nome.'.php';
        }

        public function cadastrar($id=NULL) {
            $listaCidades = $this->modelo->getLista('cidade'); //Receber dados da tabela estado

            $acao = $this->link . '&acao=gravar';
            if($id!=NULL){
                $registro = $this->modelo->getById($id);
                // var_dump($registro); die();
            }
            require $GLOBALS['APPPATH'] . '/visao/'.$this->nome.'/form_'.$this->nome.'.php';
        }


        public function gravar() {
            $dados = $_POST;
            if($_REQUEST['id']){
                $dados['id'] = $_REQUEST['id'];
            }
            // print_r($dados); die();
            $r = $this->modelo->inserir($dados);
            $this->redirect('?controlador='.$this->nome);
            // header('location: ./cidade.php');
        }

        public function remover($id) {
            // echo $id; die();
            $r = $this->modelo->remover($id);
            $this->redirect('?controlador='.$this->nome);
            // header('location: ./cidade.php');
        }
    }

 ?>
