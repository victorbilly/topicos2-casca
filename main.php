<?php

    $APPPATH = basename('.');
    require_once $APPPATH . '/controlador/controlador_base.php';
    require_once $APPPATH . '/modelo/DAO.php';
    /**
     *
     */
    class APP {

        function __construct() {}

        public function dispatcher($rota) {

            if(FILE_EXISTS($GLOBALS['APPPATH'] . '/controlador/'.$rota.'.php')){
                //faz o import do controladr
                require_once($GLOBALS['APPPATH'] . '/controlador/'.$rota.'.php');

                $str_controlador = ucfirst($rota);

                //instância do objeto
                $controller = new $str_controlador();

                $acao = $_REQUEST['acao']?? 'index';
                $id   = isset($_REQUEST['id'])? $_REQUEST['id'] : null;
                $controller->$acao($id);
            }
        }


    }

 ?>
