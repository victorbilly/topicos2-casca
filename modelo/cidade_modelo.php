<?php

    //?ATENÇÃO
    // require_once '../modelo/DAO.php';

    /**
     *
     */
    class Cidade_Modelo extends DAO {

        function __construct() {
            parent::__construct('cidade');
        }

        public function get() {
            $sql   = "select c.id, c.nome, e.UF as UF from cidade c
                        INNER JOIN estado e ON e.id=c.id_estado";
            $query = $this->bd->query($sql);
            $lista = $query->fetchAll();

            return $lista;
        }

        // public function getById($id) {
        //     $query = $this->bd->query("select * from cidade where id = " . $id);
        //     return $query->fetch();
        // }

        // public function inserir($dados) {
        //     if(isset($dados['id'])){
        //         $tmp = $dados;
        //         unset($tmp['id']);
        //         $sql = "update cidade set ";
        //         $cont=0;
        //         foreach($tmp as $key => $value){
        //             if($cont++) $sql .= ', ';
        //             $sql .= $key . ' = :' . $key;
        //         }
        //         $sql .= " where id = :id";
        //     }else{
        //         $keys   = implode(",", array_keys($dados));
        //         $values = ':' . implode(",", array_values($dados));
        //         $sql = "insert into cidade(".$keys.") values(:nome, :UF)";
        //     }
        //
        //     $query  = $this->bd->prepare($sql);
        //     $result =  $query->execute($dados);
        //     if(!$result){
        //         var_dump($query->errorInfo());
        //         die();
        //     }
        //     return true;
        // }

        // public function remover($id) {
        //     $query  = $this->bd->prepare("delete from cidade where id = :id");
        //     $query->bindParam('id', $id);
        //     $result = $query->execute();
        //     if(!$result){
        //         var_dump($query->errorInfo());
        //         die();
        //     }
        //     return true;
        // }
    }

 ?>
